<h1 align="center">SlideToUnlock</h1>
A Discord Theme inspired by 2007-2012 Design Characteristics (This is a WIP theme. Not all elements have been covered.)

# ![screenshot](https://raw.githubusercontent.com/SlippingGitty/SlideToUnlock/main/screenshots/screenshot%20update.png)

**How to install:**

* Learn how to use [BeautifulDiscord](https://github.com/leovoel/BeautifulDiscord), [Powercord](https://github.com/powercord-org/powercord), [Goosemod](https://goosemod.com/), or [BetterDiscord](https://github.com/rauenzi/BetterDiscordApp).

* Download [SlideToUnlock.user.css](https://raw.githubusercontent.com/SlippingGitty/SlideToUnlock/main/SlideToUnlock.user.css) and either
  * inject the CSS file with BeautifulDiscord
  * place in your BetterDiscord themes folder
* Powercord and Vizality users
  * open a terminal in your themes folder and type `git clone https://github.com/SlippingGitty/Discord-2012-Theme/`
* Goosemod users
  * Download [SlideToUnlockTheme.js](https://raw.githubusercontent.com/SlippingGitty/SlideToUnlock/main/SlideToUnlockTheme.js) and import the file from the "Local Modules" tab.
